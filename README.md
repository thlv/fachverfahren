# Idee
In der öffentlichen Verwaltung kommen zahlreiche [elektronische Fachverfahren](https://de.wikipedia.org/wiki/Elektronisches_Fachverfahren) (FV) zum Einsatz.

In diesem Repository möchten wir Informationen zu den FV sammeln. Ausgangspunkt ist die Überlegung, ob sich ein FV gut in ein Open-Source-Ökosystem einfügen lassen. Dazu werden die FV um Kriterien ergänzt.
Beispielsweise ist es wichtig einschätzen zu können, ob ein FV über offene, standardisierte Schnittstellen (z.B. REST-API) verfügt, Daten in offenen Dokumentformaten verarbeiten kann, oder ob der Quellcode des FV selbst öffentlich zugänglich ist.

Nach Möglichkeit soll Daten nach [Linked Open Data](https://de.wikipedia.org/wiki/Linked_Open_Data) Prinzipien bestehende Datenbestände integrieren und mit zusätzlichen Informationen anreichern.

Jedes Fachverfahren wird in einer eigenen YAML-Datei gepflegt.

# label/tags
Zur flexiblen Zuordnung von Kriterien, die ein FV erfüllt, werden folgende Tags aus der Wikidata Ontologie verwendet:

| Label | Beschreibung | Ontologie
|-------|--------------|-----------
| API | verfügt über eine [Application Programming Interface](https://de.wikipedia.org/wiki/Programmierschnittstelle) | [Q165194](https://www.wikidata.org/wiki/Special:EntityData/Q165194)
| ODF | kann Daten im [Open Document](https://de.wikipedia.org/wiki/OpenDocument) Format verarbeiten | [Q184473](https://www.wikidata.org/wiki/Special:EntityData/Q184473)
| CSV | kann Daten im [Comma-separated Values](https://de.wikipedia.org/wiki/CSV_(Dateiformat)) Format verarbeiten | [Q935809](https://www.wikidata.org/wiki/Special:EntityData/Q935809)

Eventuell existieren bereits weitere Taxonomien/Ontologien, die verwendet werden könnten.

# Feedback
Für Anregungen und Feedback wenden Sie sich gern an: sebastian.schieke@tfm.thueringen.de
